package builder

import "go.mongodb.org/mongo-driver/bson"

// Add 添加一个默认的隐式and操作
func (qb *QueryBuilder) Add(opName string, value interface{}) *QueryBuilder {
	qb.q[opName] = value
	return qb
}

// Or 添加一个 or 操作
func (qb *QueryBuilder) Or(filters ...bson.M) *QueryBuilder {
	if qb.q["$or"] == nil {

		return qb
	}

	return qb
}
