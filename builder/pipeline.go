package builder

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// AddFields do something
func (b *PipelineBuilder) AddFields(filter bson.M) *PipelineBuilder {
	b.pipeline = append(b.pipeline, bson.D{{"$addFields", filter}})
	return b
}

// Match 过滤匹配
func (b *PipelineBuilder) Match(filter bson.M) *PipelineBuilder {
	b.pipeline = append(b.pipeline, bson.D{{"$match", filter}})
	return b
}

// Sort 排序
func (b *PipelineBuilder) Sort(filter bson.M) *PipelineBuilder {
	b.pipeline = append(b.pipeline, bson.D{{"$sort", filter}})
	return b
}

// Group 分组
func (b *PipelineBuilder) Group(filter bson.M) *PipelineBuilder {
	b.pipeline = append(b.pipeline, bson.D{{"$group", filter}})
	return b
}

// Project do something
func (b *PipelineBuilder) Project(filter bson.M) *PipelineBuilder {
	b.pipeline = append(b.pipeline, bson.D{{"$project", filter}})
	return b
}

// Limit do something
func (b *PipelineBuilder) Limit(limit int) *PipelineBuilder {
	b.pipeline = append(b.pipeline, bson.D{{"$limit", limit}})
	return b
}

// Skip do something
func (b *PipelineBuilder) Skip(limit int) *PipelineBuilder {
	b.pipeline = append(b.pipeline, bson.D{{"$skip", limit}})
	return b
}

// Count 统计
func (b *PipelineBuilder) Count(fieldName string) *PipelineBuilder {
	b.pipeline = append(b.pipeline, bson.D{{"$count", fieldName}})
	return b
}

func (b *PipelineBuilder) Other(d ...bson.D) *PipelineBuilder {
	b.pipeline = append(b.pipeline, d...)
	return b
}

func (b *PipelineBuilder) Build() mongo.Pipeline {
	return b.pipeline
}
