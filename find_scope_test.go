package mdbc

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"

	"github.com/sirupsen/logrus"
	"gitlab.com/gotk/gotk/utils"
	"go.mongodb.org/mongo-driver/bson"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func TestFindScope(t *testing.T) {
	codec := bson.NewRegistryBuilder()

	codec.RegisterCodec(reflect.TypeOf(&timestamppb.Timestamp{}), &TimestampCodec{})

	client, err := ConnInit(&Config{
		URI:             "mongodb://mdbc:mdbc@10.0.0.135:27117/admin",
		MinPoolSize:     32,
		ConnTimeout:     10,
		RegistryBuilder: codec,
	})
	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	InitDB(client.Database("heywoods_golang_jingliao_crm_dev"))

	var m = NewModel(&ModelRobot{})

	var record []*ModelRobot
	var count int64
	err = m.Find().SetFilter(bson.M{
		ModelRobotField.GetStatusField(): 11,
	}).SetSort(bson.D{
		{
			Key:   ModelRobotField.GetWechatIdField(),
			Value: -1,
		},
	}).WithCount(&count).SetLimit(10).GetList(&record)
	if err != nil {
		panic(err)
	}
	logrus.Infof("count: %+v", count)
	logrus.Infof("list: %+v", utils.PluckString(record, "NickName"))
	//p1 := utils.PluckString(record, "NickName")
	//logrus.Infof("all: %+v", p1)
	//
	//err = m.Find().SetFilter(nil).SetSort(bson.D{
	//	{
	//		Key:   ModelRobotField.GetWechatIdField(),
	//		Value: -1,
	//	},
	//}).SetSkip(0).SetLimit(10).GetList(&record)
	//if err != nil {
	//	panic(err)
	//}
	//p1 = utils.PluckString(record, "NickName")
	//logrus.Infof("p1: %+v", p1)
	//
	//err = m.Find().SetFilter(nil).SetSort(bson.D{
	//	{
	//		Key:   ModelRobotField.GetWechatIdField(),
	//		Value: -1,
	//	},
	//}).SetSkip(10).SetLimit(10).GetList(&record)
	//if err != nil {
	//	panic(err)
	//}
	//p2 := utils.PluckString(record, "NickName")
	//logrus.Infof("p2: %+v", p2)
}

func TestFindScope_GetMap(t *testing.T) {
	var m = NewModel(&ModelSchedTask{})
	var record = make(map[string]*ModelSchedTask)
	err := m.Find().
		SetFilter(nil).SetLimit(2).SetCacheFunc("Id", DefaultFindCacheFunc()).GetMap(&record, "Id")
	if err != nil {
		panic(err)
	}
	marshal, err := json.Marshal(record)
	if err != nil {
		return
	}
	fmt.Printf("%+v\n", string(marshal))
}

func TestFindScope_SetCacheFunc(t *testing.T) {
	var record []*ModelSchedTask
	var m = NewModel(&ModelSchedTask{})
	err := m.Find().SetCacheFunc("Id", DefaultFindCacheFunc()).GetList(&record)
	if err != nil {
		return
	}
}
