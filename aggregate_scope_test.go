package mdbc

import (
	"fmt"
	"testing"

	"github.com/sirupsen/logrus"
	"gitlab.com/gotk/mdbc/builder"
	"go.mongodb.org/mongo-driver/bson"
)

func TestAggregateScope(t *testing.T) {
	cfg := &Config{
		URI:         "mongodb://mdbc:mdbc@10.0.0.135:27117/admin",
		MinPoolSize: 32,
		ConnTimeout: 10,
	}
	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
	client, err := ConnInit(cfg)

	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	Init(client).InitDB(client.Database("mdbc"))

	var m = NewModel(&ModelSchedTask{})

	bu := builder.NewBuilder().Pipeline()
	bu.Match(bson.M{"created_at": 0}).
		Group(bson.M{"_id": "$task_type", "count": bson.M{"$sum": 1}})

	type Res struct {
		Count int    `bson:"count"`
		Type  string `bson:"_id"`
	}

	//ds := []bson.D{{{"$match", bson.M{"created_at": bson.M{"$gt": 0}}}}}

	var records Res
	err = m.SetDebug(true).
		Aggregate().SetPipeline(bu.Build()).GetOne(&records)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", records)
}

func TestAggregateScope_GetMap(t *testing.T) {
	cfg := &Config{
		URI:         "mongodb://admin:admin@10.0.0.135:27017/admin",
		MinPoolSize: 32,
		ConnTimeout: 10,
	}
	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
	client, err := ConnInit(cfg)

	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	Init(client).InitDB(client.Database("mdbc"))

	var m = NewModel(&ModelSchedTask{})
	bu := builder.NewBuilder()
	//bu.Match(bson.M{"created_at": bson.M{"$gt": 0}}).
	//	Group(bson.M{"_id": "$task_type", "count": bson.M{"$sum": 1}}).
	//	Project(bson.M{"type": "$_id", "_id": 0, "count": 1}).
	//	Sort(bson.M{"count": 1})
	type Res struct {
		Count int    `bson:"count"`
		Type  string `bson:"type"`
	}
	record := make(map[string]*Res)
	err = m.
		SetDebug(true).
		Aggregate().SetPipeline(bu.Pipeline()).GetMap(&record, "Type")
	if err != nil {
		panic(err)
	}
	fmt.Println(record)
}
