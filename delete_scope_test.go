package mdbc

import (
	"context"
	"fmt"
	"testing"

	"github.com/sirupsen/logrus"

	"go.mongodb.org/mongo-driver/bson"
)

func TestDeleteScope_OneID(t *testing.T) {
	cfg := &Config{
		URI:         "mongodb://10.0.0.135:27117/mdbc",
		MinPoolSize: 32,
		ConnTimeout: 10,
	}
	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
	client, err := ConnInit(cfg)

	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	InitDB(client.Database("heywoods_golang_jingliao_crm_dev"))
	var m = NewModel(&ModelWsConnectRecord{})

	one, err := m.Delete().SetContext(context.Background()).SetID("697022b263e2c1528f32a26e704e63d6").One()
	if err != nil {
		logrus.Errorf("get err: %+v", err)
		return
	}

	fmt.Println(one)
}

func TestDeleteScope_OneFilter(t *testing.T) {
	cfg := &Config{
		URI:         "mongodb://10.0.0.135:27117/mdbc",
		MinPoolSize: 32,
		ConnTimeout: 10,
	}
	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
	client, err := ConnInit(cfg)

	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	InitDB(client.Database("heywoods_golang_jingliao_crm_dev"))
	var m = NewModel(&ModelWsConnectRecord{})

	one, err := m.Delete().SetContext(context.Background()).SetFilter(bson.M{}).One()
	if err != nil {
		logrus.Errorf("get err: %+v", err)
		return
	}

	fmt.Println(one)
}

func TestDeleteScope_Many(t *testing.T) {
	cfg := &Config{
		URI:         "mongodb://10.0.0.135:27117/mdbc",
		MinPoolSize: 32,
		ConnTimeout: 10,
	}
	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
	client, err := ConnInit(cfg)

	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	InitDB(client.Database("heywoods_golang_jingliao_crm_dev"))
	var m = NewModel(&ModelSchedTask{})

	//objs := []primitive.ObjectID{primitive.NewObjectID(), primitive.NewObjectID()}
	ms := []*ModelSchedTask{
		{
			Id:        "f36e55a5e4e64cc2947ae8c8a6333f6e",
			TaskState: 3,
		},
		{
			Id:        "bc227d25df1552ab4eca2610295398e4",
			TaskState: 2,
		},
		{
			Id:        "2ad97d5d330b20af0ad2ab7fd01cf32a",
			TaskState: 0,
		},
	}
	one, err := m.SetDebug(true).Delete().SetContext(context.Background()).
		SetFilter(ms, "Id", "TaskState").Many()
	//one, err := m.SetDebug(true).Delete().SetContext(context.Background()).Many()
	if err != nil {
		panic(err)
	}

	fmt.Println(one)
}

func TestDeleteScope_Other(t *testing.T) {
	cfg := &Config{
		URI:         "mongodb://10.0.0.135:27117/mdbc",
		MinPoolSize: 32,
		ConnTimeout: 10,
	}
	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
	client, err := ConnInit(cfg)

	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	InitDB(client.Database("heywoods_golang_jingliao_crm_dev"))
	var m = NewModel(&ModelSchedTask{})
	//通过id删除
	one, err := m.Delete().SetContext(context.Background()).SetIDs([]string{
		"2cab037c1ea1a96e4010b397afe703b9",
		"bd13a92ff734b2912920c5baa677432b",
		"435f56b94ba4f6387dca9b081c58f93b",
	}).Many()
	if err != nil {
		panic(err)
	}
	fmt.Println(one)

	//通过条件删除
	//one, err = m.Delete().SetContext(context.Background()).SetFilter(bson.M{"_id": "13123"}).One()
	//if err != nil {
	//	panic(err)
	//}
	//fmt.Println(one)
	//
	////设置DeleteOption
	//one, err = m.Delete().SetContext(context.Background()).SetDeleteOption(options.DeleteOptions{
	//	Collation: nil,
	//	Hint:      nil,
	//}).SetID("aac0a95ddfc5c3344777bef62bb8baae").One()
	//if err != nil {
	//	panic(err)
	//}
	//fmt.Println(one)
}
