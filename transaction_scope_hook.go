package mdbc

//import (
//	"context"
//	"github.com/opentracing/opentracing-go"
//	"github.com/opentracing/opentracing-go/ext"
//)

//// TrFunc 用于执行一次事务用到的执行函数
//// 在该回调函数内不要进行 提交/回滚
//type TrFunc func(tr *TransactionScope) error
//
//// TrHook 事务钩子 对每一个事务都执行该方法而不是对每一个会话
//type TrHook interface {
//	// BeforeTransaction 执行事务之前 需要做的操作 当error时不执行事务
//	// 其执行时机是在会话创建之后 事务开启之后尚为执行之前
//	BeforeTransaction(ctx context.Context, ts *TransactionScope) error
//	// AfterTransaction 执行事务之后 需要做的操作 error需要自行处理 不会对提交的事务构成变更
//	AfterTransaction(ctx context.Context, ts *TransactionScope) error
//}
//
//// AddHook 添加钩子
//func (tr *TransactionScope) AddHook(th TrHook) *TransactionScope {
//	tr.hooks = append(tr.hooks, th)
//	return tr
//}
//
//// OpentracingHook 链路追踪钩子 将该事务的id和操作记录起来
//// 当然你可以自己实现 但这里提供一个常规的同用的案例
//type OpentracingHook struct{}
//
//// BeforeTransaction 执行事务之前 需要做的操作 当error时不执行事务
//// 其执行时机是在会话创建之后 事务开启之后尚为执行之前
//func (oh *OpentracingHook) BeforeTransaction(ctx context.Context, ts *TransactionScope) error {
//	txID := ts.GetTrID()
//	span := trace.ObtainChildSpan(ctx, "mdbc::transaction")
//	span.SetTag("txid", txID)
//	ctx = context.WithValue(ctx, opentracingKey, span)
//	return nil
//}
//
//// AfterTransaction 执行事务之后 需要做的操作 error需要自行处理 不会对提交的事务构成变更
//func (oh *OpentracingHook) AfterTransaction(ctx context.Context, ts *TransactionScope) error {
//	spanRaw := ctx.Value(opentracingKey)
//	if spanRaw == nil {
//		return nil
//	}
//
//	span, ok := spanRaw.(opentracing.Span)
//	if !ok {
//		return nil
//	}
//
//	// 失败了 标记一下
//	if ts.Error() != nil {
//		ext.Error.Set(span, true)
//	}
//
//	span.Finish()
//	return nil
//}
