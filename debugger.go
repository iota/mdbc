package mdbc

import (
	"fmt"
	"os"
	"time"
)

// 语句执行解析器

// actionDo 实现对 操作部份 的字符化 方便debug输出
type actionDo interface {
	doString() string
}

type Debugger struct {
	collection string        // 操作集合
	errMsg     string        // 错误信息
	execT      time.Duration // 执行时间
	action     actionDo      // 执行语句
}

// String 输出执行的基于query的语句
// 思路：语句分三部分 集合部份；操作部份；附加条件部份
// 集合操作部份 这个可以直接从collection中获取集合名称
// 拼接操作部份 这是大头部份 需要每个操作实现 actionDo 接口 拼接出入参
// 附加条件部份 对于一些sort skip limit等参数进行拼接
func (d *Debugger) String() {
	_, _ = fmt.Fprintf(os.Stdout, "db.getCollection(\"%s\").%s; execTime: %s\n", d.collection, d.action.doString(), d.execT.String())
}

// GetString 获取执行的SQL信息
func (d *Debugger) GetString() string {
	return fmt.Sprintf("db.getCollection(\"%s\").%s;", d.collection, d.action.doString())
}

// ErrorString 输出执行的基于query的语句
// 暂时不考虑执行时长
func (d *Debugger) ErrorString() {
	queryAction := "╔\x1b[32mquery:\x1b[0m"
	queryMsg := fmt.Sprintf("\x1b[36m%s\x1b[0m", fmt.Sprintf("db.getCollection(\"%s\").%s;", d.collection, d.action.doString()))
	errorAction := "╠\x1b[33merror:\x1b[0m"
	errorMsg := fmt.Sprintf("\x1b[31m%s\x1b[0m", d.errMsg)
	execAction := "╚\x1b[34mexect:\x1b[0m"
	execMsg := fmt.Sprintf("\x1b[31m%s\x1b[0m", d.execT.String())
	_, _ = fmt.Fprintf(os.Stdout, "%s %s\n%s %s\n%s %s\n", queryAction, queryMsg, errorAction, errorMsg, execAction, execMsg)
}

// Echo 返回执行记录的string
func (d *Debugger) Echo() string {
	return fmt.Sprintf("db.getCollection(\"%s\").%s; execTime: %s\n", d.collection, d.action.doString(), d.execT.String())
}

//前景	背景	颜色
//30	40	黑色
//31	41	红色
//32	42	绿色
//33	43	黄色
//34	44	蓝色
//35	45	紫色
//36	46	深绿
//37	47	白色
