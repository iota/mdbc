package mdbc

//
//// trMap 事务集合 key:string(trid) value: mongo.Session
//var trMap = sync.Map{}
//
//// ctxTrIDKey 后续用于检测session与ctx是否一致
//const ctxTrIDKey = "mdbc-tx-id"
//
//// opentracingKey 用于链路追踪时span的存储
//const opentracingKey = "mdbc-tx-span"
//
//// trid 事务ID
//type trid = bson.Raw
//
//func getTrID(id trid) (string, error) {
//	raw, err := id.LookupErr("id")
//	if err != nil {
//		return "", err
//	}
//	_, uuid := raw.Binary()
//	return hex.EncodeToString(uuid), nil
//}
//
//// equalTrID 事务ID校验 检测两个id是否相同
//func equalTrID(a, b trid) bool {
//	as, aerr := a.LookupErr("id")
//	if aerr != nil {
//		return false
//	}
//	bs, berr := b.LookupErr("id")
//	if berr != nil {
//		return false
//	}
//
//	_, auuid := as.Binary()
//	_, buuid := bs.Binary()
//	return bytes.Equal(auuid, buuid)
//}
//
//// checkSession 检测session是否配置 未配置 将分配一次
//func (tr *TransactionScope) checkSession() {
//
//}
//
//// getSession 设置session
//func (tr *TransactionScope) setSession(s mongo.Session) {
//	tr.session.s = s
//}
//
//// checkTransaction 检测当前事务 未开启事务而操作事务是否将触发panic
//func (tr *TransactionScope) checkTransaction() {
//
//}
//
//// setSessionID 设置session id
//func (tr *TransactionScope) setSessionID() {
//	if tr.err != nil {
//		return
//	}
//	tr.trID = tr.getSession().ID()
//	trMap.Store(tr.GetTxID(), tr.session)
//}
//
//// deleteSessionID 删除session id
//func (tr *TransactionScope) deleteSessionID() {
//	if tr.err != nil {
//		return
//	}
//
//	trMap.Delete(tr.GetTxID())
//}
//
//// getSessionID 获取session id
//func (tr *TransactionScope) getSessionID() (mongo.Session, bool) {
//	if tr.err != nil {
//		return nil, false
//	}
//
//	res, exist := trMap.Load(tr.GetTxID())
//	if !exist {
//		return nil, false
//	}
//
//	return res.(mongo.Session), true
//}
//
//// EndSession 关闭会话
//func (tr *TransactionScope) EndSession() *TransactionScope {
//	tr.getSession().EndSession(tr.cw.ctx)
//	tr.deleteSessionID()
//	return tr
//}
//
//// StartTransaction 开启事务
//// 如果session未设置或未开启 将panic
//func (tr *TransactionScope) StartTransaction() *TransactionScope {
//	tr.checkSession()
//	tr.err = tr.getSession().StartTransaction(tr.trOpts...)
//	tr.hasStartTx = true
//	return tr
//}
//
//// GetSession 获取会话 如果未设置 session 将返回 nil
//func (tr *TransactionScope) GetSession() mongo.Session {
//	tr.checkSession()
//	return tr.getSession()
//}
//
//// GetSessionContext 获取当前会话的上下文
//func (tr *TransactionScope) GetSessionContext() mongo.SessionContext {
//	tr.checkSession()
//	return tr.session.ctx
//}
//
//// GetCollection 获取当前会话的collection
//func (tr *TransactionScope) GetCollection() *Scope {
//	return tr.scope
//}
//
//// GetTxID 获取事务ID的字符串
//func (tr *TransactionScope) GetTxID() string {
//	tr.checkSession()
//	if tr.trID == nil {
//		tr.err = fmt.Errorf("session id empty")
//		return ""
//	}
//
//	id, err := getTrID(tr.trID)
//	if err != nil {
//		tr.err = err
//		return ""
//	}
//
//	return id
//}
//
//// Error 获取执行的error
//func (tr *TransactionScope) Error() error {
//	if tr.err != nil {
//		return tr.err
//	}
//	return nil
//}
