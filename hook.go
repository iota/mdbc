package mdbc

// Hook mongodb的指令hook
type Hook interface {
	Before() error
	After() error
}

// UpdateHook 更新指令的hook
type UpdateHook interface {
	Hook
}
