package mdbc

import (
	"testing"

	"github.com/sirupsen/logrus"
)

func TestInsertScope_One(t *testing.T) {
	cfg := &Config{
		URI:         "mongodb://mdbc:mdbc@10.0.0.135:27117/admin",
		MinPoolSize: 32,
		ConnTimeout: 10,
	}
	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
	client, err := ConnInit(cfg)

	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	Init(client).InitDB(client.Database("mdbc"))

	var m = NewModel(&ModelSchedTask{})

	var record = ModelSchedTask{}

	record.TaskState = uint32(TaskState_TaskStateCompleted)
	record.Id = "insertffddddfdfdsdnkodsanfkasdf"

	res, err := m.SetDebug(true).Insert().One(&record)
	if err != nil {
		panic(err)
	}
	logrus.Infof("res: %+v", res.GetString())
}

func TestInsertScope_Many(t *testing.T) {
	cfg := &Config{
		URI:         "mongodb://admin:admin@10.0.0.135:27017/admin",
		MinPoolSize: 32,
		ConnTimeout: 10,
	}
	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
	client, err := ConnInit(cfg)

	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	Init(client).InitDB(client.Database("mdbc"))

	var m = NewModel(&ModelSchedTask{})

	var list = []interface{}{
		&ModelSchedTask{
			Id: "insertManyaaaajkfnjaksdf",
		},
		&ModelSchedTask{
			Id: "insertManybbbwdsfsadnasjkf",
		},
	}

	res, err := m.SetDebug(true).Insert().Many(list)
	if err != nil {
		panic(err)
	}
	logrus.Infof("res: %+v", res.GetListString())
}
