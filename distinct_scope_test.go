package mdbc

import (
	"testing"

	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
)

func TestDistinctScope(*testing.T) {
	cfg := &Config{
		URI:         "mongodb://admin:admin@10.0.0.135:27017/admin",
		MinPoolSize: 32,
		ConnTimeout: 10,
	}
	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
	client, err := ConnInit(cfg)

	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	InitDB(client.Database("mdbc"))

	var m = NewModel(&ModelSchedTask{})

	var data []string

	err = m.
		SetDebug(true).
		Distinct().
		SetCacheFunc("task_type", DefaultDistinctCacheFunc()).
		SetFieldName("task_type").
		SetFilter(bson.M{"task_type": bson.M{"$ne": "test"}}).
		Get(&data)

	if err != nil {
		panic(err)
	}
	logrus.Infof("get ttl: %+v\n", m.cache.ttl)
	logrus.Infof("res %+v\n", data)
}
