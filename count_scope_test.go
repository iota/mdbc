package mdbc

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/sirupsen/logrus"

	"go.mongodb.org/mongo-driver/bson"
)

func TestCountScope_Count(t *testing.T) {
	cfg := &Config{
		URI:         "mongodb://admin:admin@10.0.0.135:27017/admin",
		MinPoolSize: 32,
		ConnTimeout: 10,
	}
	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
	client, err := ConnInit(cfg)

	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	Init(client).InitDB(client.Database("mdbc"))

	var m = NewModel(&ModelSchedTask{})
	count, err := m.Count().SetContext(context.Background()).
		SetFilter(bson.M{"created_at": bson.M{"$gt": 0}}).
		SetSkip(1).
		SetLimit(10).
		SetMaxTime(1 * time.Second).
		Count()
	if err != nil {
		panic(err)
		return
	}

	fmt.Println(count)
}
